##############################################################################
# Rotation Centre - Toggle Inkscape rotation centre between centre of mass / bounding box centre
# An Inkscape 1.2.1+ extension
# Appears under Extensions>Modify Path>Rotation Centre
##############################################################################
