#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) [2023] [Matt Cottam], [mpcottam@raincloud.co.uk]
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#

##############################################################################
# Rotation Centre - Toggle Inkscape rotation centre between centre of mass / bounding box centre
# An Inkscape 1.2.1+ extension
# Appears under Extensions>Modify Path>Rotation Centre
##############################################################################

import inkex
from inkex.bezier import cspcofm
from inkex import PathElement


def draw_crosshair(self, x, y, size):

    x1 = x2 = x
    y1 = y - size
    y2 = y + size
    x3 = x - size
    x4 = x + size
    y3 = y4 = y

    # Make the crosshair path
    d = f'M {x1} {y1} L {x2} {y2} M {x3} {y3} L {x4} {y4}'

    crosshair_path = PathElement()
    crosshair_path.set('d', d)
    crosshair_path.set('stroke', 'black')
    crosshair_path.set('stroke-width', 0.5)

    return crosshair_path

def get_cofm(self, current_path):

    duplicate_path = current_path.duplicate()

    x_rot_offset = current_path.bounding_box().left + current_path.bounding_box().width / 2
    y_rot_offset = current_path.bounding_box().top + current_path.bounding_box().height / 2

    rot_x = current_path.bounding_box().center.x - x_rot_offset
    rot_y = current_path.bounding_box().center.y - y_rot_offset

    if self.options.cofm_close_path_cb:

        d = duplicate_path.get('d')

        if d.strip()[-1].upper() != 'Z':
            d = d + f' Z'

        duplicate_path.set('d', d)

    csp = duplicate_path.path.transform(current_path.composed_transform()).to_superpath()

    tx, ty = cspcofm(csp)

    inverse_transform = -current_path.getparent().composed_transform()
    object_tx, object_ty = inverse_transform.apply_to_point([tx, ty])

    cofm_rot_x = rot_x + object_tx - x_rot_offset
    cofm_rot_y = rot_y - object_ty + y_rot_offset

    duplicate_path.delete()

    # object_tx, object_ty are absolute coordinates for use in an untransformed layer.
    return object_tx, object_ty, cofm_rot_x, cofm_rot_y



class RotationCentre(inkex.EffectExtension):

    def add_arguments(self, pars):

        pars.add_argument("--rotation_center_type_radio", type=str, dest="rotation_center_type_radio", default='reset')

        pars.add_argument("--cofm_close_path_cb", type=inkex.Boolean, dest="cofm_close_path_cb", default=True)

        pars.add_argument("--cofm_crosshair_cb", type=inkex.Boolean, dest="cofm_crosshair_cb", default=False)
    
    def effect(self):

        selection_list = self.svg.selected
        if len(selection_list) < 1 or selection_list[0].TAG != 'path':
            inkex.errormsg('Please select at least 1 path')
            return

        current_path = selection_list[0]

        if self.options.rotation_center_type_radio != 'cofm':

            current_path.set('inkscape:transform-center-x', None)
            current_path.set('inkscape:transform-center-y', None)
            return

        object_tx, object_ty, cofm_rot_x, cofm_rot_y = get_cofm(self, current_path)

        current_path.set('inkscape:transform-center-x', cofm_rot_x)
        current_path.set('inkscape:transform-center-y', cofm_rot_y)

        if self.options.cofm_crosshair_cb:
            crosshair_path = draw_crosshair(self, object_tx, object_ty, 5)
            self.svg.get_current_layer().append(crosshair_path)


if __name__ == '__main__':
    RotationCentre().run()
